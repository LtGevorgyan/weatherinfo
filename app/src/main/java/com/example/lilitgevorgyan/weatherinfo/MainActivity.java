package com.example.lilitgevorgyan.weatherinfo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_expandable_list_item_1,
                getResources().getStringArray(R.array.names));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 1:
                        new GetData((TextView)findViewById(R.id.textView)).execute("Yerevan");
                        break;
                    case 2:
                        new GetData((TextView)findViewById(R.id.textView)).execute("Moscow");
                        break;
                    case 3:
                        new GetData((TextView)findViewById(R.id.textView)).execute("Washington");
                        break;
                    case 4:
                        new GetData((TextView)findViewById(R.id.textView)).execute("Paris");
                        break;
                    case 5:
                        new GetData((TextView)findViewById(R.id.textView)).execute("London");
                        break;
                    case 6:
                        new GetData((TextView)findViewById(R.id.textView)).execute("Berlin");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
