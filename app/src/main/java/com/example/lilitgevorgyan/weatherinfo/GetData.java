package com.example.lilitgevorgyan.weatherinfo;

import android.os.AsyncTask;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Lilit Gevorgyan on 2/6/2018.
 */

public class GetData extends AsyncTask<String, Void, String> {
    private TextView mTextView;

    public GetData(TextView textView){
        this.mTextView = textView;
    }

    @Override
    protected String doInBackground(String... params) {
        String result = "";
        HttpURLConnection conn = null;
        try {
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q="+ URLEncoder.encode(params[0], "UTF-8")+"&APPID=ea574594b9d36ab688642d5fbeab847e");
            conn = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            if (in != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                String line = "";

                while ((line = bufferedReader.readLine()) != null)
                    result += line;
            }
            in.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(conn!=null)
                conn.disconnect();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        try {
            JSONObject jsonObj = new JSONObject(result);
            JSONObject obj = new JSONObject(jsonObj.getString("main"));
            JSONObject jsonObject = new JSONObject(jsonObj.getString("wind"));
            mTextView.setText("Temperature is a -> " + obj.getString("temp")+ " Kelvin" + "\n" +
                    "Minimal temperture is a -> " + obj.getString("temp_min") + " Kelvin" + "\n" +
                    "Maximal temperature is a -> " + obj.getString("temp_max") + " Kelvin" + "\n" +
                    "Wind speed is a -> " + jsonObject.getString("speed") );

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
